Simple setup to compare EPG/bloch simulations to spin-echo ground truth measurements.
This repository provides registered spin-echo and MRF measurements and a simple visualization tool to compare the signals.
The visualization tool looks like the image below and works interactively. Just run the file main.m.
No other dependencies are required. Sequence parameters can be found in U.mat. For example flip angles are in U.Sequence.Flip_angles and TR in U.Sequence.Repetition_time.

![](image1.png)