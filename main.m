%% Load all the required data to inspect the parameter fits
% cg_recon = conjugate gradient sense reconstruction  [Nx Ny 1 1 Ntimepoints]
% recon    = nufft reconstruction                     [Nx Ny 1 1 Ntimepoints]
% dict     = dictionary filled with EPG simulations   [Ntissues Ntimepoints]
% idx      = lookup table with T1/T2 values           [T1/T2/B1 Ntissues]
% Qmaps_se = spin echo measurements maps              [Nx Ny T1/T2]
% pos_se   = indices to feed to idx                   [1 Nx*Ny]
%
% Small notes, dictionary values with T2 > 0.5 * T1 were filled with zeros.
% You will see this back in the visualization. So this is not a bug. 
% U.mat contains the metadata to simulate the sequence if required. 
% The raw k-space data as removed to reduce the size, but you can find the raw data here :
% /nfs/bsc01/researchData/USER/tbruijne/MR_Data/Internal_data/Fingerprinting_data/U2/20171031_Phantom_MRF/Scan4/U.mat

load('/nfs/bsc01/researchData/USER/tbruijne/Projects_Software/MRF_fully_sampled/Images.mat')
load('/nfs/bsc01/researchData/USER/tbruijne/Projects_Software/MRF_fully_sampled/Dictionary.mat')
load('/nfs/bsc01/researchData/USER/tbruijne/Projects_Software/MRF_fully_sampled/Qmaps_se.mat')

% Dictionary match & reconstruct MRF maps
cg_recon   = reshape(cg_recon,[],size(recon,5));
A          = cg_recon * dict';
[c,pos]    = max(A,[],2);
cg_recon   = reshape(cg_recon,size(recon,1),size(recon,1),1,1,size(recon,5));

Qmaps_mrf(:,:,1) = reshape(idx(1,pos),size(recon,1),[]);
Qmaps_mrf(:,:,2) = reshape(idx(2,pos),size(recon,1),[]);
Qmaps_mrf(:,:,3) = reshape(idx(3,pos),size(recon,1),[]);
Qmaps_mrf(:,:,4) = reshape(c,size(recon,1),[]);

% Visualization of measured vs matched signal evolutions
% Click on the pixel of interest to get interactive line updates
mrf_vs_se_fingerprints(cg_recon,Qmaps_mrf,Qmaps_se,dict,c,pos,pos_se);

