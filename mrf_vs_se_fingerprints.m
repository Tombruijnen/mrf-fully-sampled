function mrf_vs_se_fingerprints(recon,Qmaps_mrf,Qmaps_se,dict,c,idx_mrf,idx_se)
%%Compare spin-echo measurement vs MRF measurements
idim = size(recon);
if idim(3) > 1 || idim(4) > 1 || numel(idim) <5
    display(['Recon should have input [N N 1 1 nt]'])
    return;
end

% Forward generation
mrf_synth_recon = zeros([prod(idim(1:2)) idim(5)]);
se_synth_recon = zeros([prod(idim(1:2)) idim(5)]);
for n = 1 : size(mrf_synth_recon,1)
    mrf_synth_recon(n,:)    = dict(idx_mrf(n),:) * c(n);
    se_synth_recon(n,:)     = dict(idx_se(n),:) * c(n);
end
mrf_synth_recon = reshape(mrf_synth_recon,size(recon));
se_synth_recon  = reshape(se_synth_recon,size(recon));

figure,
%subplot(221);imshow(abs(recon(:,:,:,:,round(end/2))),[]);
s1 = subplot(221);imshow(cat(2,Qmaps_mrf(:,:,1),Qmaps_se(:,:,1)),[0 2]);colormap(s1,'inferno');title('Click using the crosshair on a pixel.','Color','w')
s2 = subplot(222);imshow(cat(2,Qmaps_mrf(:,:,2),Qmaps_se(:,:,2)),[0 .25]);colormap(s2,'inferno');title('Click using the crosshair on a pixel.','Color','w')
set(gcf, 'Position', get(0, 'Screensize'),'Color','k');
while 1
    [y,x] = ginput(1);
    x = round(x);y = round(y);
    if y > size(recon,2)
        y = y - size(recon,2);
    end
    s1 = subplot(221);imshow(cat(2,Qmaps_mrf(:,:,1),Qmaps_se(:,:,1)),[0 2]);colormap(s1,'inferno');title(['MRF: ',num2str(Qmaps_mrf(x,y,1),'%.2f'),' ms','  |  ','SE: ',num2str(Qmaps_se(x,y,1),'%.2f'),' ms'],'Color','w');c1=colorbar;c1.Color = 'w'
    s2 = subplot(222);imshow(cat(2,Qmaps_mrf(:,:,2),Qmaps_se(:,:,2)),[0 .25]);colormap(s2,'inferno');title(['MRF: ',num2str(Qmaps_mrf(x,y,2),'%.3f'),' ms','  |  ','SE: ',num2str(Qmaps_se(x,y,2),'%.3f'),' ms'],'Color','w');c2=colorbar;c2.Color = 'w'
    subplot(2,2,3:4);plot(squeeze(abs(recon(x,y,:,:,:))),'k');hold on;
    plot(squeeze(abs(mrf_synth_recon(x,y,:,:,:))),'b','LineWidth',2);
    plot(squeeze(abs(se_synth_recon(x,y,:,:,:))),'r','LineWidth',2);hold off
    xlabel('Timepoints [-]');ylabel('|Signal intensity|')
    set(gca,'LineWidth',3,'Color',[.85 .85 .85],'XColor','w','YColor','w','XTick',0:200:1000,'FontWeight','bold')
    legend('Raw signal','MRF','SE')
    
    grid on;box on;
end
% END
end